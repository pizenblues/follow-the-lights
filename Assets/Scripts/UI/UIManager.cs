﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour{
    public EquipmentUI equipmentDetails;
    public PlayerController mouseControl;
    public GameObject pauseUI, 
        inventoryUI,
        settingsUI,
        equipmentDetailsUI, 
        equipmentSimpleUI, 
        lifeBarUI, 
        firstItemInInventory, 
        firstItemInPauseMenu,
        firstItemInSettingsMenu;
    bool isPaused, inventoryMenuIsOn, pauseMenuIsOn, settingsMenuIsOn;

    void Start(){
        isPaused = false;
        inventoryMenuIsOn = false;
        pauseMenuIsOn = false;
    }

    void Update(){
        if(Input.GetKeyDown(KeyCode.Q)){
            showInventory();
        }

        if(Input.GetKeyDown(KeyCode.Escape)){
            showPauseMenu();
        }
    }

    public void showInventory(){
        if(isPaused == false & inventoryMenuIsOn == false & pauseMenuIsOn == false){
            pauseGame();
            inventoryUI.SetActive(true);
            equipmentDetailsUI.SetActive(true);
            equipmentDetails.UpdatePlayerStats();
            equipmentSimpleUI.SetActive(false);
            inventoryMenuIsOn = !inventoryMenuIsOn;
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(firstItemInInventory);
        }else if(inventoryMenuIsOn == true & isPaused == true & pauseMenuIsOn == false){
            hideInventory();
        }
    }

    public void hideInventory(){
        inventoryUI.SetActive(false);
        equipmentDetailsUI.SetActive(false);
        equipmentSimpleUI.SetActive(true);
        inventoryMenuIsOn = !inventoryMenuIsOn;
        resumeGame();

    }

    public void showPauseMenu(){
        if(isPaused == false & inventoryMenuIsOn == false & pauseMenuIsOn == false & settingsMenuIsOn == false){
            pauseGame();
            pauseUI.SetActive(true);
            equipmentSimpleUI.SetActive(false);
            lifeBarUI.SetActive(false);
            pauseMenuIsOn = !pauseMenuIsOn;
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(firstItemInPauseMenu);
        }else if(pauseMenuIsOn == true & isPaused == true & inventoryMenuIsOn == false){
            hidePauseMenu();
        }
    }

    public void hidePauseMenu(){
        if(settingsMenuIsOn){
            hideSettingsMenu();
        }
        pauseUI.SetActive(false);
        equipmentSimpleUI.SetActive(true);
        lifeBarUI.SetActive(true);
        pauseMenuIsOn = !pauseMenuIsOn;
        resumeGame();
    }

    public void showSettingsMenu(){
        settingsMenuIsOn = !settingsMenuIsOn;
        pauseUI.SetActive(false);
        settingsUI.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firstItemInSettingsMenu);
    }

    public void hideSettingsMenu(){
        settingsUI.SetActive(false);
        settingsMenuIsOn = !settingsMenuIsOn;
    }

    public void pauseGame(){
        if(isPaused == false){
            Time.timeScale = 0;
            mouseControl.enabled = false;
            isPaused = true;
        }else{
            resumeGame();
        }
    }

    public void resumeGame(){
        Time.timeScale = 1f;
        isPaused = false;
        mouseControl.enabled = true;
    }
}