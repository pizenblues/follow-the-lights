﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class mainMenuUI : MonoBehaviour{
    public GameObject firstButtonInMenu;

    private void Start() {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firstButtonInMenu);
    }

    public void exitGame(){
        Application.Quit();
    }

    public void startGame(){
        SceneManager.LoadScene (sceneName:"plaza");
    }

    public void settings(){
        //SceneManager.LoadScene (sceneName:"StartMenu");
    }
}
