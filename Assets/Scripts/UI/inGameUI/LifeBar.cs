﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBar : MonoBehaviour{
    public Slider slider;

    public void SetMaxValue(int i){
        slider.value = i;
        slider.maxValue = i;
    }

    public void SetcurrentHealth(int j){
        slider.value = j;
    }
}
