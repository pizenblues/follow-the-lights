﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour{
    public int maxHealth;
    public int currentHealth;
    public Stat attack;
    public Stat armor;
    public Stat speed;

    void Awake(){
        currentHealth = maxHealth;
    }

    public void TakeDamage(int damage){
        damage -= armor.GetValue();
        damage = Mathf.Clamp(damage, 0, int.MaxValue);
        Debug.Log("damage taken " + damage);
        currentHealth -= damage;
        UpdateHealth();
        if(currentHealth <= 0){
            Dies();
        }
    }

    public void Heal(int cure){
        if(currentHealth == maxHealth){
            return;
        }else{
            int newCurrentHealth = cure + currentHealth;
            if(newCurrentHealth > maxHealth){
                int totalCure = (newCurrentHealth - maxHealth);
                currentHealth += totalCure;
            }else{
                currentHealth = newCurrentHealth;
            }
            UpdateHealth();
        }
    }

    public virtual void UpdateHealth(){}

    public virtual void Dies(){
        Debug.Log("is deado");
    }
}