﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentMeshControl : MonoBehaviour{

    public Transform player;
    Equipment currentItem;
    GameObject instantiatedPrefab;
    public void attachItemToPlayer(Equipment currentItem){
        //get the position of the player and add the desired position (left hand!)
        Vector3 newPosition = currentItem.equipedPosition + player.transform.position;
        instantiatedPrefab = Instantiate(currentItem.mesh, newPosition, currentItem.equipedRotation, player);
        GameObject currentObject = instantiatedPrefab;
        currentItem.objectInScene = currentObject;
    }

    public void deattachtemToPlayer(Equipment currentItem){
        Destroy(currentItem.objectInScene);
    }
}