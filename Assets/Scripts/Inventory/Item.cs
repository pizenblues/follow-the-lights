﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item name", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    new public string name = "Item name";
    public Sprite icon = null;
    public string typeOfItem = null;
    public string description;
    public int quantity = 1;

    // esto deberia ir en equipment
    public int defenseModifier;
    public int attackModifier;
    public int speedModifier;
    
    public bool isEquipment = false;
    public bool isDefaultItem = false;

    public virtual void UseItem(){}

    public virtual void UnEquipThisItem(){}
}
