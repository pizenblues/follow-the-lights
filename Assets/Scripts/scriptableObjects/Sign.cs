﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "sign name", menuName = "CustomObject/Sign")]
public class Sign : ScriptableObject{
    new public string name = "sign name";
    public Sprite image;
}
