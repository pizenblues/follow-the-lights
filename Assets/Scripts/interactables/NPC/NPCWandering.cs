﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCWandering : MonoBehaviour {
    private NavMeshAgent _navMeshAgent;
    public Transform[] patrolPoints;
    private int currentControlPointIndex = 0;

    void Awake(){
        _navMeshAgent = GetComponent<NavMeshAgent>();
        MoveToNextPatrolPoint();
    }

    void Update(){
        if (_navMeshAgent.enabled){
            bool patrol = false;

            patrol = patrolPoints.Length > 0;
            if (patrol){
                if (!_navMeshAgent.pathPending && _navMeshAgent.remainingDistance < 0.5f)
                    MoveToNextPatrolPoint();
            }

            // _animator.SetBool("Run", follow || patrol);
        }
    }

    void MoveToNextPatrolPoint(){
        if (patrolPoints.Length > 0){
            _navMeshAgent.destination = patrolPoints[currentControlPointIndex].position;
            currentControlPointIndex++;
            currentControlPointIndex %= patrolPoints.Length;
        }
    }
}
