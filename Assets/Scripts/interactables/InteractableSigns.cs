﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InteractableSigns : Interactable{

    public GameObject panel, directionLeft, directionRight, directionUp, directionDown;
    public TextMeshProUGUI signName;
    public Image signImage;
    public bool left, right, up, down;
    public Sign sign;

    void Update(){
        if(isFocus){
            if(Input.GetKeyDown(KeyCode.E)){
                HidePanel();
            }
            if( (Input.GetKeyDown(KeyCode.Q)) || (Input.GetKeyDown(KeyCode.Escape))){
                HidePanel();
            }
        }
    }

    public override void ShowPanel(){
        signName.text = sign.name;
        signImage.sprite = sign.image;
        panel.SetActive(true);
        if(left){
            // directionRight.SetActive(false);
            directionLeft.SetActive(true);
        }else if(right){
            // directionLeft.SetActive(false);
            directionRight.SetActive(true);
        }else if(up){
            // directionLeft.SetActive(false);
            directionUp.SetActive(true);
        }else if(down){
            // directionLeft.SetActive(false);
            directionDown.SetActive(true);
        }
        Debug.Log("up");
    }

    public override void HidePanel(){
        panel.SetActive(false);
        Debug.Log("down");
    }
}
